'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var fs = require('fs');
var child_process = require('child_process');
var fileHelpers = require('file-helpers');

var MAINDOCDTDOPTIONS = {
  doctype: {
    sysID: 'http://www.calligra.org/DTD/krita-2.0.dtd',
    pubID: '-//KDE//DTD krita 2.0//EN'
  }
};

var DOCUMENTINFODTDOPTIONS = {
  doctype: {
    sysID: 'http://www.calligra.org/DTD/document-info-1.1.dtd',
    pubID: '-//KDE//DTD document-info 1.1//EN'
  }
};

/**
 * 
 * @param {Object} documentInfoJSON 
 * @param {string} folderPath 
 */
function makeDocumentInfoXML(documentInfoJSON, folderPath) {
  return fileHelpers.JSONToXML(documentInfoJSON, folderPath + '/documentinfo.xml', MAINDOCDTDOPTIONS);
}

/**
 * 
 * @param {Object} mainDocumentJSON 
 * @param {string} folderPath 
 */
function makeMainDocumentXML(mainDocumentJSON, folderPath) {
  return fileHelpers.JSONToXML(mainDocumentJSON, folderPath + '/maindoc.xml', MAINDOCDTDOPTIONS);
}

function readFilePromise(filePath) {
  return new Promise(function (resolve, reject) {
    fs.readFile(filePath, function (err, data) {
      if (err) reject(Error(err));
      resolve(data.toString());
    });
  });
}

/**
 * 
 * @param {string} folderPath 
 */
function getDocumentInfoXML(folderPath) {
  return readFilePromise(folderPath + '/documentinfo.xml');
}

/**
 * 
 * @param {string} folderPath 
 */
function getMainDocumentXML(folderPath) {
  return readFilePromise(folderPath + '/maindoc.xml');
}

/**
 * 
 * @param {string} folderPath
 */
function getDocumentInfo(folderPath) {
  return new Promise(function (resolve, reject) {
    getDocumentInfoXML(folderPath).then(function (data) {
      return fileHelpers.XMLToJSON(data);
    }).then(function (json) {
      resolve(json);
    }).catch(function (err) {
      reject(Error(err));
    });
  });
}

/**
 * 
 * @param {string} folderPath
 */
function getMainDocument(folderPath) {
  return new Promise(function (resolve, reject) {
    getMainDocumentXML(folderPath).then(function (data) {
      return fileHelpers.XMLToJSON(data);
    }).then(function (json) {
      resolve(json);
    }).catch(function (err) {
      reject(Error(err));
    });
  });
}

/**
 * 
 * @param {string} filePath 
 * @param {string} sourceFolder 
 * @param {string} convertedFolder 
 */
function getConvertedFilePath(filePath, sourceFolder, convertedFolder) {
  var convertedFilePath = fileHelpers.changeFolder(filePath, sourceFolder, convertedFolder);
  return fileHelpers.changeExtension(convertedFilePath, '.kra', '.png');
}

/**
 * 
 * @param {string} filePath 
 * @param {string} convertedFilePath 
 * @param {string} kritaPath 
 * @param {string} settings 
 */
function convertFile(filePath, convertedFilePath, kritaPath) {
  return new Promise(function (resolve, reject) {
    fileHelpers.createFilePathSync(convertedFilePath);
    var options = [filePath, '--export', '--export-filename', convertedFilePath];
    var kritaProcess = child_process.spawn(kritaPath, options);

    kritaProcess.on('close', function () {
      resolve();
    });

    kritaProcess.on('error', function (err) {
      reject(Error(err));
    });
  });
}

exports.MAINDOCDTDOPTIONS = MAINDOCDTDOPTIONS;
exports.DOCUMENTINFODTDOPTIONS = DOCUMENTINFODTDOPTIONS;
exports.makeDocumentInfoXML = makeDocumentInfoXML;
exports.makeMainDocumentXML = makeMainDocumentXML;
exports.getDocumentInfo = getDocumentInfo;
exports.getMainDocument = getMainDocument;
exports.getConvertedFilePath = getConvertedFilePath;
exports.convertFile = convertFile;
