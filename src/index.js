import { readFile } from 'fs';
import { spawn } from 'child_process';

import { JSONToXML, XMLToJSON, changeFolder, changeExtension, createFilePathSync } from 'file-helpers';


export const MAINDOCDTDOPTIONS = {
  doctype: {
    sysID: 'http://www.calligra.org/DTD/krita-2.0.dtd',
    pubID: '-//KDE//DTD krita 2.0//EN',
  },
};

export const DOCUMENTINFODTDOPTIONS = {
  doctype: {
    sysID: 'http://www.calligra.org/DTD/document-info-1.1.dtd',
    pubID: '-//KDE//DTD document-info 1.1//EN',
  },
};

/**
 * 
 * @param {Object} documentInfoJSON 
 * @param {string} folderPath 
 */
export function makeDocumentInfoXML(documentInfoJSON, folderPath) {
  return JSONToXML(documentInfoJSON, `${folderPath}/documentinfo.xml`, MAINDOCDTDOPTIONS);
}

/**
 * 
 * @param {Object} mainDocumentJSON 
 * @param {string} folderPath 
 */
export function makeMainDocumentXML(mainDocumentJSON, folderPath) {
  return JSONToXML(mainDocumentJSON, `${folderPath}/maindoc.xml`, MAINDOCDTDOPTIONS);
}

function readFilePromise(filePath) {
  return new Promise(function (resolve, reject) {
    readFile(filePath, (err, data) => {
      if (err) reject(Error(err));
      resolve(data.toString());
    });
  });
}

/**
 * 
 * @param {string} folderPath 
 */
function getDocumentInfoXML(folderPath) {
  return readFilePromise(folderPath + '/documentinfo.xml');
}

/**
 * 
 * @param {string} folderPath 
 */
function getMainDocumentXML(folderPath) {
  return readFilePromise(folderPath + '/maindoc.xml');
}

/**
 * 
 * @param {string} folderPath
 */
export function getDocumentInfo(folderPath) {
  return new Promise((resolve, reject) => {
    getDocumentInfoXML(folderPath)
      .then(data => XMLToJSON(data))
      .then(json => { resolve(json); })
      .catch(err => { reject(Error(err)); });
  });
}

/**
 * 
 * @param {string} folderPath
 */
export function getMainDocument(folderPath) {
  return new Promise((resolve, reject) => {
    getMainDocumentXML(folderPath)
      .then(data => XMLToJSON(data))
      .then(json => { resolve(json); })
      .catch(err => { reject(Error(err)); });
  });
}

/**
 * 
 * @param {string} filePath 
 * @param {string} sourceFolder 
 * @param {string} convertedFolder 
 */
export function getConvertedFilePath(filePath, sourceFolder, convertedFolder) {
  const convertedFilePath = changeFolder(filePath, sourceFolder, convertedFolder);
  return changeExtension(convertedFilePath, '.kra', '.png');
}

/**
 * 
 * @param {string} filePath 
 * @param {string} convertedFilePath 
 * @param {string} kritaPath 
 * @param {string} settings 
 */
export function convertFile(filePath, convertedFilePath, kritaPath) {
  return new Promise((resolve, reject) => {
    createFilePathSync(convertedFilePath);
    const options = [filePath, '--export', '--export-filename', convertedFilePath];
    const kritaProcess = spawn(kritaPath, options);

    kritaProcess.on('close', () => {
      resolve();
    });

    kritaProcess.on('error', (err) => {
      reject(Error(err));
    });
  });
}
