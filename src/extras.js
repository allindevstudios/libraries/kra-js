import { read as readImage } from 'jimp';

export function autoCrop(imagePath) {
  return new Promise((resolve, reject) => {
    readImage(imagePath).then((image) => {
      const croppedImage = image.autocrop();
      croppedImage.write(imagePath, (err) => {
        if (err) reject(Error(err));
        resolve();
      });
    }).catch(err => reject(Error(err)));
  });
}

export function applyFilter(filter) {
  console.log(`${filter} can't be applied yet`);
}
