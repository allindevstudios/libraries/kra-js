// maybe change name?
import {
  changeExtension,
  changeFolder,
  findAllFilesInFolderSync,
  makeTempFolder,
  unzipToFolder,
  sleep,
  JSONToXML,
  XMLToJSON,
} from 'file-helpers';

import {
  convertFile,
  
} from './kra-helpers';

const SETTINGS = {
  sourceFolder: '',
  targetFolder: '',
  kritaExecutablePath: '',
  targetFolderLayers: '',
  lastEditedJson: '',
  tempFolder: makeTempFolder('kekxporter'),
};

const setSettings = (obj) => {
  if (!obj) {
    throw new Error('Please pass a settings object!');
  }
  if (!obj.sourceFolder) {
    throw new Error('sourceFolder is required!');
  }
  SETTINGS.sourceFolder = obj.sourceFolder;

  if (!obj.targetFolder) {
    throw new Error('targetFolder is required!');
  }
  SETTINGS.targetFolder = obj.targetFolder;

  if (!obj.kritaExecutablePath) {
    throw new Error('kritaExecutablePath is required!');
  }
  SETTINGS.kritaExecutablePath = obj.kritaExecutablePath;

  if (!obj.targetFolderLayers) {
    obj.targetFolderLayers = obj.targetFolder;
  }
  SETTINGS.targetFolderLayers = obj.targetFolderLayers;

  if (!obj.lastEditedJson) {
    obj.lastEditedJson = `${SETTINGS.sourceFolder}/last_edited.json`;
  }

  if (obj.tempFolder) {
    SETTINGS.tempFolder = obj.tempFolder;
  } else {
    SETTINGS.tempFolder = makeTempFolder('kekxporter');
  }
};

const getSettings = () => SETTINGS;

const findAllKritaFilesInProject = () => findAllFilesInFolderSync(SETTINGS.sourceFolder, '.kra');

async function importProject() {
  return new Promise((resolve, reject) => {
    const files = findAllKritaFilesInProject();

    // choose files depending on
    // files = await filterFilesWithEdit() or o
    // but meh cba now.
    // DONE
    // unzip to tempfolder
    const promises = files.map(async (file) => {
      let unzippedFolder = changeFolder(file, SETTINGS.sourceFolder, SETTINGS.tempFolder);
      unzippedFolder = changeExtension(unzippedFolder, '.kra', '');

      return new Promise((resolve, reject) => {
        unzipToFolder(file, unzippedFolder).then(() => sleep(300)).then(() => {
          //
          // read XML and expose data per file
          const XmlFile = `${unzippedFolder}/maindoc.xml`;
          return XMLToJSON(XmlFile);
        }).then((json) => {
          json.DOC.IMAGE[0].layers[0].layer.forEach((layer) => {
            layer.$.visible = '0';
          });
          resolve(json);
        })
          .catch((err) => {
            reject(Error(err));
          });
      });
    });

    Promise.all(promises).then((jsons) => {
      const obj = {
        files,
        jsons,
      };
      resolve(obj);
    }).catch((err) => {
      reject(Error(err));
    });
  });
}

// doesn't have to resolve any data so...
async function exportProject(obj) {
  // pass a settings or whatever object that is which layer to export.
  // This is the export all option I guess.
  return new Promise(((resolve, reject) => {
    if (!obj.settings) {
      const files = obj.files;
      const jsons = obj.jsons;
      const indexes = Array.from(Array(files.length).keys());
      // export all layers
      const promises = indexes.map(async (i) => {
        const json = jsons[i];
        const file = files[i];

        console.log('JSON', JSON.stringify(json));

        convertFile(file, SETTINGS.targetFolderLayers, SETTINGS.sourceFolder, SETTINGS.kritaExecutablePath);

        let unzippedFolder = changeFolder(file, SETTINGS.sourceFolder, SETTINGS.tempFolder);
        unzippedFolder = changeExtension(unzippedFolder, '.kra', '');
        console.log(`Exporting ${file} to ${unzippedFolder} and then to ${SETTINGS.targetFolderLayers}`);
        // return ExportAllLayers(unzippedFolder, json, SETTINGS.targetFolderLayers, SETTINGS.tempFolder, SETTINGS.kritaExecutablePath);
      });

      Promise.all(promises).then((results) => {
        resolve();
      }).catch((err) => {
        reject(Error(err));
      });
    }
  }));
}

module.exports = {
  setSettings,
  getSettings,
  importProject,
  exportProject,
};
