// TODO: THIS!

const fs = require('fs');

const kekxporter = require('./kekxporter');

const settings = {
  sourceFolder: '/home/laurent/Private/All-in/Libraries/kra-js/images_source_folder',
  targetFolder: '/home/laurent/Projects/All-in Development Studios/Libraries/kra-js/images_destination_folder',
  kritaExecutablePath: '/home/laurent/Downloads/krita-3.1.4-x86_64.appimage',
  targetFolderLayers: '/home/laurent/Projects/All-in Development Studios/Libraries/kra-js/images_destination_folder',
};

function saveToFile(filename, content) {
  return new Promise(((resolve, reject) => {
    fs.writeFile(filename, content, (err) => {
      if (err) reject(Error(err));
      resolve();
    });
  }));
}

kekxporter.setSettings(settings);

console.log(kekxporter.getSettings());

kekxporter.importProject().then((obj) => {
  console.log('Start of export!');
  return kekxporter.exportProject(obj);
  // return saveToFile('derp.json', JSON.stringify(obj))
}).then(() => {
  console.log('DONE!');
}).catch((err) => {
  console.log(err);
  throw Error(err);
});
